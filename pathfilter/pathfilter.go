// Package pathfilter provides path filtering functions.
//
// Analyzer CLIs built on top of https://github.com/urfave/cli/v2
// might use this package to exclude files from the scan.
// MakeFlags returns CLI flags users can set to configure the filter,
// and NewFilter configures a path filter based on the CLI context.
//
package pathfilter

import (
	"path/filepath"

	"github.com/urfave/cli/v2"
)

const flagExcludedPaths = "excluded-paths"

// MakeFlags returns the CLI flags used to specify excluded paths.
// Given prefix is prepended to the environment variable.
func MakeFlags(envPrefix string) []cli.Flag {
	return []cli.Flag{
		&cli.StringSliceFlag{
			Name:    flagExcludedPaths,
			EnvVars: []string{envPrefix + "EXCLUDED_PATHS"},
			Usage:   "Comma-separated list of paths (globs supported) to be excluded from the output.",
		},
	}
}

// NewFilter returns a filter for a given CLI context.
func NewFilter(c *cli.Context) (*Filter, error) {
	paths := c.StringSlice(flagExcludedPaths)
	if err := validatePatterns(paths); err != nil {
		return nil, err
	}
	return &Filter{paths}, nil
}

// validatePatterns returns an error if any of the given path is invalid.
// The filepath package does not expose any function to check the syntax
// of a match pattern, so validatePatterns simply calls filepath.Match looking for errors.
// It wraps filepath.ErrBadPattern to add some context to the syntax error.
// filepath.Match won't return any error unless it's given a non empty string.
func validatePatterns(paths []string) error {
	for _, p := range paths {
		if _, err := filepath.Match(p, "x"); err != nil {
			return ParseError{p}
		}
	}
	return nil
}
