package issue

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/require"
)

var testSASTIssue = Issue{
	Category:    CategorySast,
	Name:        "Possible command injection",
	Message:     "Possible command injection in application_controller",
	Description: "The ciphertext produced is susceptible to alteration by an adversary. This mean that the cipher provides no wayto detect that the data has been tampered with. If the ciphertext can be controlled by an attacker, it couldbe altered without detection.\n\nThe solution is to used a cipher that includes a Hash basedMessage Authentication Code (HMAC) to sign the data. Combining a HMAC function to the existing cipher is proneto error [1] ( http://www.thoughtcrime.org/blog/the-cryptographic-doom-principle/ ).Specifically, it is always recommended that you be able to verify the HMAC first, and only if the data is unmodified,do you then perform any cryptographic functions on the data.\n\nThe following modes are vulnerablebecause they don't provide a HMAC:",
	CompareKey:  "09abf078636daa3bc6a3d49fba232774a5d9b7c454e1003e6ab12cd56938296a",
	Location: Location{
		File:      "app/controllers/application_controller.rb",
		LineStart: 831,
		LineEnd:   832,
	},
	Scanner: Scanner{
		ID:   "brakeman",
		Name: "Brakeman",
	},
	Severity:             SeverityLevelMedium,
	Confidence:           ConfidenceLevelHigh,
	Solution:             "Use the system(command, parameters) method which passes command line parameters safely.",
	RawSourceCodeExtract: "system(params[:cmd])",
	Identifiers: []Identifier{
		CVEIdentifier("CVE-2018-1234"),
	},
	Links: []Link{
		{
			Name: "Awesome-security blog post",
			URL:  "https://example.com/blog-post",
		},
		{
			URL: "https://example.com/another-blog-post",
		},
	},
}

const testSASTIssueJSON = `{
  "id": "8a3b1088f586727598108a0a696c0be98fc9b4329fca2525d1e10cc0f30fcea8",
  "category": "sast",
  "name": "Possible command injection",
  "message": "Possible command injection in application_controller",
  "description": "The ciphertext produced is susceptible to alteration by an adversary. This mean that the cipher provides no wayto detect that the data has been tampered with. If the ciphertext can be controlled by an attacker, it couldbe altered without detection.\n\nThe solution is to used a cipher that includes a Hash basedMessage Authentication Code (HMAC) to sign the data. Combining a HMAC function to the existing cipher is proneto error [1] ( http://www.thoughtcrime.org/blog/the-cryptographic-doom-principle/ ).Specifically, it is always recommended that you be able to verify the HMAC first, and only if the data is unmodified,do you then perform any cryptographic functions on the data.\n\nThe following modes are vulnerablebecause they don't provide a HMAC:",
  "cve": "09abf078636daa3bc6a3d49fba232774a5d9b7c454e1003e6ab12cd56938296a",
  "severity": "Medium",
  "confidence": "High",
  "solution": "Use the system(command, parameters) method which passes command line parameters safely.",
  "raw_source_code_extract": "system(params[:cmd])",
  "scanner": {
    "id": "brakeman",
    "name": "Brakeman"
  },
  "location": {
    "file": "app/controllers/application_controller.rb",
    "start_line": 831,
    "end_line": 832
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2018-1234",
      "value": "CVE-2018-1234",
      "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
    }
  ],
  "links": [
    {
      "name": "Awesome-security blog post",
      "url": "https://example.com/blog-post"
    },
    {
      "url": "https://example.com/another-blog-post"
    }
  ]
}`

var testDependencyScanningVulnerability = DependencyScanningVulnerability{Issue{
	Category: CategoryDependencyScanning,
	Scanner: Scanner{
		ID:   "gemnasium",
		Name: "Gemnasium",
	},
	Location: Location{
		File: "app/pom.xml",
		Dependency: &Dependency{
			Package: Package{
				Name: "io.netty/netty",
			},
			Version: "3.9.1.Final",
		},
	},
	Identifiers: []Identifier{
		CVEIdentifier("CVE-2018-1234"),
	},
}}

const testDependencyScanningVulnerabilityJSON = `{
  "id": "6bada84603f1ce4b286e970bdff5df0386413c920206eb4eccfab14ef9014804",
  "category": "dependency_scanning",
  "message": "Vulnerability in io.netty/netty",
  "cve": "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
  "scanner": {
    "id": "gemnasium",
    "name": "Gemnasium"
  },
  "location": {
    "file": "app/pom.xml",
    "dependency": {
      "package": {
        "name": "io.netty/netty"
      },
      "version": "3.9.1.Final"
    }
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2018-1234",
      "value": "CVE-2018-1234",
      "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
    }
  ]
}`

const testDependencyScanningIssueJSON = `{
  "category": "dependency_scanning",
  "scanner": {
    "id": "gemnasium",
    "name": "Gemnasium"
  },
  "location": {
    "file": "app/pom.xml",
    "dependency": {
      "package": {
        "name": "io.netty/netty"
      },
      "version": "3.9.1.Final"
    }
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2018-1234",
      "value": "CVE-2018-1234",
      "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
    }
  ]
}`

var testContainerScanningIssue = Issue{
	Category:    CategoryContainerScanning,
	Message:     "CVE-2019-9511 in nghttp2",
	Description: "Some HTTP/2 implementations are vulnerable to window size manipulation and stream prioritization manipulation, potentially leading to a denial of service. The attacker requests a large amount of data from a specified resource over multiple streams. They manipulate window size and stream priority to force the server to queue the data in 1-byte chunks. Depending on how efficiently this data is queued, this can consume excess CPU, memory, or both.",
	CompareKey:  "debian:10:nghttp2:CVE-2019-9511",
	Severity:    SeverityLevelHigh,
	Solution:    "Upgrade nghttp2 from 1.36.0-2 to 1.36.0-2+deb10u1",
	Scanner: Scanner{
		ID:   "klar",
		Name: "klar",
	},
	Location: Location{
		Dependency: &Dependency{
			Package: Package{
				Name: "nghttp2",
			},
			Version: "1.36.0-2",
		},
		OperatingSystem: "debian:10",
		Image:           "registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e",
	},
	Identifiers: []Identifier{
		{
			Type:  "cve",
			Name:  "CVE-2019-9511",
			Value: "CVE-2019-9511",
			URL:   "https://security-tracker.debian.org/tracker/CVE-2019-9511",
		},
	},
	Links: []Link{
		{
			Name: "",
			URL:  "https://security-tracker.debian.org/tracker/CVE-2019-9511",
		},
	},
}

const testContainerScanningIssueJSON = `{
  "id": "e59f97db4e8335aa45f595adeeeacb4c75bdcc995b08f32b5a3bea7c9dc8f63d",
  "category": "container_scanning",
  "message": "CVE-2019-9511 in nghttp2",
  "description": "Some HTTP/2 implementations are vulnerable to window size manipulation and stream prioritization manipulation, potentially leading to a denial of service. The attacker requests a large amount of data from a specified resource over multiple streams. They manipulate window size and stream priority to force the server to queue the data in 1-byte chunks. Depending on how efficiently this data is queued, this can consume excess CPU, memory, or both.",
  "cve": "debian:10:nghttp2:CVE-2019-9511",
  "severity": "High",
  "solution": "Upgrade nghttp2 from 1.36.0-2 to 1.36.0-2+deb10u1",
  "scanner": {
    "id": "klar",
    "name": "klar"
  },
  "location": {
    "dependency": {
      "package": {
        "name": "nghttp2"
      },
      "version": "1.36.0-2"
    },
    "operating_system": "debian:10",
    "image": "registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e"
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2019-9511",
      "value": "CVE-2019-9511",
      "url": "https://security-tracker.debian.org/tracker/CVE-2019-9511"
    }
  ],
  "links": [
    {
      "url": "https://security-tracker.debian.org/tracker/CVE-2019-9511"
    }
  ]
}`

const testEmptyIssueJSON = `{
  "id": "7e4eeda55620d655f351aec0ee4596fe9dcf61941ab4c72a76b67c22792674ac",
  "category": "",
  "cve": "",
  "scanner": {
    "id": "",
    "name": ""
  },
  "location": {},
  "identifiers": null
}`

var testSecretDetectionIssue = Issue{
	Category:    CategorySecretDetection,
	Name:        "AWS API key",
	Message:     "AWS API key",
	Description: "Amazon Web Services API key detected; please remove and revoke it if this is a leak.",
	CompareKey:  "testdata/main.go:8a22accb113d641b78b389ccce92fca96acbe2fd4f1701ead6783768fdbe9d8a:AWS",
	Severity:    SeverityLevelCritical,
	Confidence:  ConfidenceLevelUnknown,
	Scanner: Scanner{
		ID:   "gitleaks",
		Name: "Gitleaks",
	},
	Location: Location{
		File: "testdata/main.go",
		Commit: &Commit{
			Sha:     "90be2fe7f76e44cc708261b6c646b5dc546d47fb",
			Author:  "johndoe@gitlab.com",
			Message: "This is a commit message",
			Date:    "Tue May 12 12:20:51 2020 -0400",
		},
	},
	Identifiers: []Identifier{
		{
			Type:  "gitleaks_rule_id",
			Name:  "Gitleaks rule ID AWS",
			Value: "AWS",
		},
	},
}

const testSecretDetectionIssueJSON = `{
  "id": "38c71186ff345883138f9628be3a5fa797f2b63a0163f72735b5cea89f0ab73f",
  "category": "secret_detection",
  "name": "AWS API key",
  "message": "AWS API key",
  "description": "Amazon Web Services API key detected; please remove and revoke it if this is a leak.",
  "cve": "testdata/main.go:8a22accb113d641b78b389ccce92fca96acbe2fd4f1701ead6783768fdbe9d8a:AWS",
  "severity": "Critical",
  "confidence": "Unknown",
  "scanner": {
    "id": "gitleaks",
    "name": "Gitleaks"
  },
  "location": {
    "file": "testdata/main.go",
    "commit": {
      "author": "johndoe@gitlab.com",
      "date": "Tue May 12 12:20:51 2020 -0400",
      "message": "This is a commit message",
      "sha": "90be2fe7f76e44cc708261b6c646b5dc546d47fb"
    }
  },
  "identifiers": [
    {
      "type": "gitleaks_rule_id",
      "name": "Gitleaks rule ID AWS",
      "value": "AWS"
    }
  ]
}`

var testCoverageFuzzingIssue = Issue{
	Category:    CategoryCoverageFuzzing,
	Name:        "Heap-buffer-overflow READ of 0x1337",
	Message:     "Heap-buffer-overflow READ",
	Description: "Heap-buffer-overflow READ of 0x1337",
	Severity:    SeverityLevelMedium,
	Confidence:  ConfidenceLevelUnknown,
	Scanner: Scanner{
		ID:   "libfuzzer",
		Name: "libFuzzer",
	},
	Location: Location{
		CrashAddress:      "0xABABDCDC",
		CrashType:         "Heap-buffer-overflow READ",
		CrashState:        "func1+0xa\nfunc2+0xc\nfunc3+0xa",
		StacktraceSnippet: "full stacktrace is func1+0xabababab\nfunc2+0xabababab\nfunc3+0xababcddcd",
	},
	Identifiers: []Identifier{
		{
			Type:  IdentifierTypeCWE,
			Name:  "Heap-buffer-overflow READ",
			Value: "CWE-125",
			URL:   "https://cwe.mitre.org/data/definitions/125.html",
		},
	},
}

const testCoverageFuzzingIssueJSON = `{
  "id": "3bfdc3048a9de05c6180c60fdb545cda9cef63810fea125cf3fd07c86b6de350",
  "category": "coverage_fuzzing",
  "name": "Heap-buffer-overflow READ of 0x1337",
  "message": "Heap-buffer-overflow READ",
  "description": "Heap-buffer-overflow READ of 0x1337",
  "cve": "",
  "severity": "Medium",
  "confidence": "Unknown",
  "scanner": {
    "id": "libfuzzer",
    "name": "libFuzzer"
  },
  "location": {
    "crash_address": "0xABABDCDC",
    "crash_type": "Heap-buffer-overflow READ",
    "crash_state": "func1+0xa\nfunc2+0xc\nfunc3+0xa",
    "stacktrace_snippet": "full stacktrace is func1+0xabababab\nfunc2+0xabababab\nfunc3+0xababcddcd"
  },
  "identifiers": [
    {
      "type": "cwe",
      "name": "Heap-buffer-overflow READ",
      "value": "CWE-125",
      "url": "https://cwe.mitre.org/data/definitions/125.html"
    }
  ]
}`

func TestIssue(t *testing.T) {
	t.Run("ID", func(t *testing.T) {
		var tcs = []struct {
			Name          string
			Vulnerability Issue
			ID            string
		}{
			{
				Name:          "SAST",
				Vulnerability: testSASTIssue,
				ID:            "8a3b1088f586727598108a0a696c0be98fc9b4329fca2525d1e10cc0f30fcea8",
			},
			{
				Name:          "DS",
				Vulnerability: testDependencyScanningVulnerability.ToIssue(),
				ID:            "6bada84603f1ce4b286e970bdff5df0386413c920206eb4eccfab14ef9014804",
			},
			{
				Name:          "Empty",
				Vulnerability: Issue{},
				ID:            "7e4eeda55620d655f351aec0ee4596fe9dcf61941ab4c72a76b67c22792674ac",
			},
			{
				Name:          "CS",
				Vulnerability: testContainerScanningIssue,
				ID:            "e59f97db4e8335aa45f595adeeeacb4c75bdcc995b08f32b5a3bea7c9dc8f63d",
			},
			{
				Name:          "Secret Detection",
				Vulnerability: testSecretDetectionIssue,
				ID:            "38c71186ff345883138f9628be3a5fa797f2b63a0163f72735b5cea89f0ab73f",
			},
			{
				Name:          "Coverage Fuzzing",
				Vulnerability: testCoverageFuzzingIssue,
				ID:            "3bfdc3048a9de05c6180c60fdb545cda9cef63810fea125cf3fd07c86b6de350",
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				got := tc.Vulnerability.ID()
				want := tc.ID
				require.Equal(t, want, got)
			})
		}
	})

	t.Run("MarshalJSON", func(t *testing.T) {
		var tcs = []struct {
			Name          string
			Vulnerability Issue
			JSON          string
		}{
			{
				Name:          "SAST",
				Vulnerability: testSASTIssue,
				JSON:          testSASTIssueJSON,
			},
			{
				Name:          "DS",
				Vulnerability: testDependencyScanningVulnerability.ToIssue(),
				JSON:          testDependencyScanningVulnerabilityJSON,
			},
			{
				Name:          "Empty",
				Vulnerability: Issue{},
				JSON:          testEmptyIssueJSON,
			},
			{
				Name:          "CS",
				Vulnerability: testContainerScanningIssue,
				JSON:          testContainerScanningIssueJSON,
			},
			{
				Name:          "Secret Detection",
				Vulnerability: testSecretDetectionIssue,
				JSON:          testSecretDetectionIssueJSON,
			},
			{
				Name:          "Coverage Fuzzing",
				Vulnerability: testCoverageFuzzingIssue,
				JSON:          testCoverageFuzzingIssueJSON,
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				b, err := json.Marshal(tc.Vulnerability)
				require.NoError(t, err)

				var buf bytes.Buffer
				json.Indent(&buf, b, "", "  ")
				got := buf.String()
				require.Equal(t, tc.JSON, got)
			})
		}
	})

	t.Run("UnmarshalJSON", func(t *testing.T) {
		var tcs = []struct {
			Name  string
			JSON  string // JSON encoded vulnerability
			Issue Issue  // Issue to be JSON decoded
		}{
			{
				Name:  "SAST",
				JSON:  testSASTIssueJSON,
				Issue: testSASTIssue,
			},
			{
				Name:  "DS",
				JSON:  testDependencyScanningIssueJSON,
				Issue: testDependencyScanningVulnerability.Issue,
			},
			{
				Name:  "ContainerScanning",
				JSON:  testContainerScanningIssueJSON,
				Issue: testContainerScanningIssue,
			},
			{
				Name:  "Empty",
				JSON:  testEmptyIssueJSON,
				Issue: Issue{},
			},
			{
				Name:  "Secret Detection",
				JSON:  testSecretDetectionIssueJSON,
				Issue: testSecretDetectionIssue,
			},
			{
				Name:  "Coverage Fuzzing",
				JSON:  testCoverageFuzzingIssueJSON,
				Issue: testCoverageFuzzingIssue,
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				var got Issue
				err := json.Unmarshal([]byte(tc.JSON), &got)
				require.NoError(t, err)
				require.Equal(t, tc.Issue, got)
			})
		}
	})
}
